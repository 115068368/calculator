package com.anyway.calculator;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;

/**
 * @author lz
 * @date 2022/1/2
 */
public class Calculator {

    /**
     * 默认精确小数点
     */
    private static final int DEFAULT_SCALE = 3;

    private static List<Command> log = new ArrayList<>();


    /**
     * 相加
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal add(BigDecimal num1, BigDecimal num2) {

        if (Objects.isNull(num1)) {
            num1 = BigDecimal.ZERO;
        }
        if (Objects.isNull(num2)) {
            num2 = BigDecimal.ZERO;
        }
        return num1.add(num2);
    }

    /**
     * 相减
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal subtract(BigDecimal num1, BigDecimal num2) {

        if (Objects.isNull(num1)) {
            num1 = BigDecimal.ZERO;
        }
        if (Objects.isNull(num2)) {
            num2 = BigDecimal.ZERO;
        }
        return num1.subtract(num2);
    }

    /**
     * 相乘
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal multiply(BigDecimal num1, BigDecimal num2) {

        if (Objects.isNull(num1)) {
            num1 = BigDecimal.ONE;
        }
        if (Objects.isNull(num2)) {
            num2 = BigDecimal.ONE;
        }
        return num1.multiply(num2);
    }

    /**
     * 相除
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal divide(BigDecimal num1, BigDecimal num2) {

        if (Objects.isNull(num1)) {
            num1 = BigDecimal.ZERO;
        }
        if (Objects.isNull(num2) || num1.equals(0)) {
            throw new IllegalArgumentException("请输入正确的分母值");
        }
        MathContext mc = new MathContext(DEFAULT_SCALE, RoundingMode.HALF_DOWN);
        return num1.divide(num2, mc);
    }

    private static Character inputCalSymbol(Scanner input) {

        System.out.println("请输入要进行的操作: +, -, *,  /");

        Character option = input.next().charAt(0);
        List<Character> list = Arrays.asList('+', '-', '*', '/');

        if (!list.contains(option)) {
            return inputCalSymbol(input);
        }
        return option;
    }

    private static BigDecimal inputDecimal(Scanner input) {

        String option = input.next();

        try {
            return new BigDecimal(option);
        } catch (Exception e) {
            System.out.println("请输入正确数字数字");
        }
        return inputDecimal(input);
    }

    private static void undo() {
        if (log.size() == 0) {
            System.out.println("回滚到上一指令......");
            return;
        }
        Command command = log.remove(log.size() - 1);
        System.out.println(command.getNum1() + command.getOps().toString() + command.getNum2() + " undo，移除日志");
    }

    private static void redo() {
        if (log.size() == 0) {
            System.out.println("没有重新需要执行的指令");
            return;
        }
        System.out.println("重新执行上一指令");
        Command command = log.get(log.size() - 1);
        execute(command);
    }

    private static Command command() {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入第一个数字");
        BigDecimal num1 = inputDecimal(input);
        Character ops = inputCalSymbol(input);
        System.out.println("请输入第二个数字");
        BigDecimal num2 = inputDecimal(input);
        return new Command(num1, ops, num2);
    }

    private static void commit(Command command) {
        System.out.println("保存执行指令结果....");
        log.add(command);
    }

    private static void execute(Command command) {

        System.out.println("开始执行指令......");
        BigDecimal result = BigDecimal.ZERO;

        BigDecimal num1 = command.getNum1();
        BigDecimal num2 = command.getNum2();

        switch (command.getOps()) {
            case '+':
                result = add(num1, num2);
                System.out.println(num1 + "+" + num2 + "=" + result);
                break;
            case '-':
                result = subtract(num1, num2);
                System.out.println(num1 + "-" + num2 + "=" + result);
                break;
            case '*':
                result = multiply(num1, num2);
                System.out.println(num1 + "*" + num2 + "=" + result);
                break;
            case '/':
                result = divide(num1, num2);
                System.out.println(num1 + "/" + num2 + "=" + result);
                break;
        }

    }

    static class Command {
        private BigDecimal num1;
        private Character ops;
        private BigDecimal num2;

        private BigDecimal result;

        public BigDecimal getNum1() {
            return num1;
        }

        public void setNum1(BigDecimal num1) {
            this.num1 = num1;
        }

        public Character getOps() {
            return ops;
        }

        public void setOps(Character ops) {
            this.ops = ops;
        }

        public BigDecimal getNum2() {
            return num2;
        }

        public void setNum2(BigDecimal num2) {
            this.num2 = num2;
        }

        public BigDecimal getResult() {
            return result;
        }

        public void setResult(BigDecimal result) {
            this.result = result;
        }

        public Command(BigDecimal num1, Character ops, BigDecimal num2) {
            this.num1 = num1;
            this.ops = ops;
            this.num2 = num2;
        }
    }

    public static void main(String[] args) throws Exception {

        while (true) {
            Command command = command();
            execute(command);
            commit(command);
            System.out.println("请确认是否继续，输入Y或者N：");
            Scanner scanner = new Scanner(System.in);
            if (scanner.next().equals("Y")) {
                continue;
            } else {
                break;
            }
        }
        redo();
        undo();

    }

}